<?php 

	//API PARAMETERS (Developer Supplied API Key) 
	define('C_APIKEY', '[YOUR_API_KEY]');

	//USE THE DEVELOPMENT ENVIRONMENT URL
	define('C_APIURL', 'https://api.dial.works/'); 

	//ENTER THE DOMAIN & URL TO SEND WEBHOOKS
	define('C_HOOK', '[YOUR_LISTENER_URL]'); 

	//Simple cURL Based Request
	function ProcessRequest($endpoint, $data='') { 

		$curl = curl_init(); 
		curl_setopt_array($curl, array( 
			CURLOPT_RETURNTRANSFER => 1, 
			CURLOPT_URL => C_APIURL.$endpoint, 
			CURLOPT_HTTPHEADER => array('APIKEY:'.C_APIKEY), 
			CURLOPT_POST => 1, 
			CURLOPT_POSTFIELDS => array('data'=>$data, 'webhook'=>C_HOOK) 
			) 
		); 

		$resp = curl_exec($curl); 
		curl_close($curl); 

		return $resp; 

	} 

	//IF AJAX BASED REQUEST && ENDPOINT/ACTION PRESENT (INCLUDE USER AUTHORIZATION)
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') && isset($_POST['endpoint'])) { 

		//OUTPUT/RETURN JSON RESPONSE 
		echo ProcessRequest($_POST['endpoint'], $_POST['data']); 

	}

?>