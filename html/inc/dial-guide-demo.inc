<!doctype html>
<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
		<script>
			$(document).ready(function(){
				$(document).on('keyup', 'input', validate);
				validate();
			});

			function validate() {
				$('#submit').attr('disabled', $('#fname').val().trim()=='' || $('#yob').val().length!=4);
			}

		</script>
		<meta name = "viewport" content = "initial-scale=1.0, maximum-scale=1.0, width=device-width"> 
		<title>The Insights Guide</title>
	</head>
	<body>
		<form method='post'>
			<h1>Start Demo</h1>
			<input type='hidden' name='package' value='demo' />
			<input type='hidden' name='user' value='<?=uniqid('u');?>' />
			<input id='fname' type='text' name='name' value='' placeholder='First Name'class='full'  />

			<select name='gender' class='half'>
				<option value='male'>Male</option>
				<option value='female'>Female</option>
			</select>
			<input id='yob' class='half' type='number' name='yob' value='' min='1900' max='2018' placeholder='YOB' />

			<input class='full' id='submit' type='submit' value='GO' disabled=true />

			<br class='clr' />
		</form>
	</body>
</html> 