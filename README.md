# README #

This repository contains the sample files to setup and
integrate the DIAL Insights Benefits Guide.

### How do I get set up? ###

1. Create a virtual host with a DOCUMENT_ROOT of /html
2. Edit the connector.php file and replace [YOUR API_KEY] with the development API key 
3. Replace [YOUR_LISTENER_URL] with the virtual host domain you created (http://www.example.com/listen.php)
4. Navigate to http://www.example.com/setup.php in your browser. If the "demo" package is not completed, create it by following the prompts.
5. Navigate to http://www.example.com/guide.php in your browser. There should be a sample user form to post user details to start the guide.

### Who do I talk to? ###

* If you have any questions, please email support@dialinsights.com for assistance *